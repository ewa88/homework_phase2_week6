﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CompanyRevenueDistribution;
using Newtonsoft.Json;
using RevenueCalculator.Interfaces;

namespace RevenueCalculator.FileDeserializer
{
  internal class FileDeserializer : IFileDeserializer
  {
      public List<CompanyRevenue> DeserializeJsonFiles()
      {
          var allFilesList = new List<CompanyRevenue>();
          var jsonFileList = new List<CompanyRevenue>();
            string filepath, fileName;
         
        for (int i = 1969; i <=2030; i++)
          {
             // fileName = "CompaniesRevenies_" + 1970 + "Kopia.json";
              fileName = "CompaniesRevenies_" + i + ".json";

                //filepath = @" C: \Users\Student13\Desktop\Homework\Homework_ETAPP_TK_3\" + fileName;
                filepath = @"..\..\..\..\" + fileName;
                if (File.Exists(filepath))
              {
                  jsonFileList = JsonConvert.DeserializeObject<CompanyRevenue[]>(File.ReadAllText(filepath)).ToList();

                  foreach (var data in jsonFileList)
                  {
                      allFilesList.Add(data);
                  }
              }
          }
            return allFilesList;  
      }

      public List<CompanyRevenue> DeserializeNecessaryFiles(int fromYear, int toYear)
      {
          var allFilesList = new List<CompanyRevenue>();
          var jsonFileList = new List<CompanyRevenue>();
          string filepath, fileName;
          for (int i = fromYear; i <= toYear; i++)
          {
             //  fileName = "CompaniesRevenies_" + i + "Kopia.json";
              fileName = "CompaniesRevenies_" + i + ".json";

              filepath = @"..\..\..\..\" + fileName;
              if (File.Exists(filepath))
              {
                  jsonFileList = JsonConvert.DeserializeObject<CompanyRevenue[]>(File.ReadAllText(filepath)).ToList();

                  foreach (var data in jsonFileList)
                  {
                      allFilesList.Add(data);
                  }
              }
          }
          return allFilesList;
      }
  }
}
