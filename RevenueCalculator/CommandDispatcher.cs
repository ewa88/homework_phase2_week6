﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using CompanyRevenueDistribution;
using RevenueCalculator.Interfaces;

namespace RevenueCalculator
{
   internal class CommandDispatcher : ICommandDispatcher
   {
        public List<IRevenueAddition> OperationList = new List<IRevenueAddition>();

        public CommandDispatcher()
        {
            AddNativeCalculations();
            AddCalculationsFromPlugins();
        }

        private void AddCalculationsFromPlugins()
        {
            var dllFileNames = GetDllFileNames();
            var assemblies = GetLoadedAssemblies(dllFileNames);
            OperationList.AddRange(GetPluggedOperations(assemblies));
        }

        private IEnumerable<string> GetDllFileNames()
        {
            const string path = @".\Plugins";
            string[] dllFileNames = { };
            if (Directory.Exists(path))
            {
                dllFileNames = Directory.GetFiles(path, "*.dll");
            }
            return dllFileNames;
        }

        private static IEnumerable<IRevenueAddition> GetPluggedOperations(IEnumerable<Assembly> assemblies)
        {
            var pluginType = typeof(IRevenueAddition);
            var pluginCalculations = new List<IRevenueAddition>();

            foreach (var assembly in assemblies)
            {
                if (assembly == null)
                {
                    continue;
                }

                var types = assembly.GetTypes();
                var pluginTypes = types.Where(type => !type.IsInterface && !type.IsAbstract &&
                                                      type.GetInterface(pluginType.FullName) != null);
                foreach (var type in pluginTypes)
                {
                    pluginCalculations.Add((IRevenueAddition)Activator.CreateInstance(type));
                }
            }

            return pluginCalculations;
        }
        private IEnumerable<Assembly> GetLoadedAssemblies(IEnumerable<string> dllFileName)
        {
            var assemblies = new List<Assembly>();
            foreach (var dllFile in dllFileName)
            {
                var asseblyName = AssemblyName.GetAssemblyName(dllFile);
                var assembly = Assembly.Load(asseblyName);
                assemblies.Add(assembly);
            }
            return assemblies;
        }

        private void AddNativeCalculations()
        {
            OperationList.Add(new RevenueAddition());
        }

        public IEnumerable<string> GetCalculationNames()
        {
            return OperationList.Select(a => a.Name);
        }
        public string GetCommand()
        {
            var commandDispatcher = new CommandDispatcher();
            Console.WriteLine("Dostępne operacje: \n");
            foreach (var calculationName in commandDispatcher.GetCalculationNames())
            {
                Console.WriteLine(calculationName + "\n");

            }
            Console.WriteLine("");
            string choice = "";
            bool operationFound = false;
            while (operationFound==false)
            {
                Console.WriteLine("Podaj nazwe operacji: \n");
                choice = Console.ReadLine();
                foreach (var operation in OperationList)
                {
                    if (operation.Name == choice)
                        operationFound = true;
                }
            }          
            return choice;
        }
    }


}
