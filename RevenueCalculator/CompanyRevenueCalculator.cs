﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompanyRevenueDistribution;
using RevenueCalculator.Interfaces;

namespace RevenueCalculator
{
   internal class CompanyRevenueCalculator : ICompanyRevenueCalculator
   {
       private IFileDeserializer _fileDeserializer;
   
       public CompanyRevenueCalculator(IFileDeserializer fileDeserializer)
       {
           _fileDeserializer = fileDeserializer;
       }
        private List<CompanyRevenue> GlobalRevenueList = new List<CompanyRevenue>();

        public void CalculateTotalRevenue(string calculationName)
        {
            var fileData = _fileDeserializer.DeserializeJsonFiles();
            CompanyRevenueCalculation(fileData.ToList(), calculationName);
            PrintAllCompanysRevenueSync();
        }
        public void CompanyRevenueCalculation(List<CompanyRevenue> companyRevenuesList, string calculationName)
        {
            var commandDispatcher = new CommandDispatcher();
            var typeOfCalculation = commandDispatcher.OperationList.SingleOrDefault(x => x.Name == calculationName);
            GlobalRevenueList = typeOfCalculation.ToSumCompanyRevenue(companyRevenuesList);
        }
        public void PrintAllCompanysRevenueSync()
        {
            foreach (var company in GlobalRevenueList)
            {
                Console.WriteLine($"{company.Name}\t{company.Value}");
            }
        }
       public void GetCompanyRevenueData(string calculationName)
       {
           bool loop = true;
           while (loop)
           {
               Console.WriteLine("Wpisz nazwe firmy (jesli chcesz zakonczyc wpisz exit):");
               int fromYear = 0, toYear = 0;
               long sumOfRevenue = 0;
               string answer = Console.ReadLine();
               if (answer == "exit")
               {
                   loop = false;
               }
               else
               {
                   bool intervalLoop = true;
                   while (intervalLoop)
                   {
                       var getString = new StringFromRegex();
                       var nextIntervalOfYear = getString.GetStringFromRegex();
                       var numbers = nextIntervalOfYear.ToCharArray();

                       fromYear = Int32.Parse($"{numbers[0]}{numbers[1]}{numbers[2]}{numbers[3]}");
                       toYear = Int32.Parse($"{numbers[5]}{numbers[6]}{numbers[7]}{numbers[8]}");


                       if (fromYear > toYear)
                       {
                           Console.WriteLine("Poczatkowa data nie moze byc starsza od koncowej");

                       }
                       else
                       {
                           intervalLoop = false;
                       }
                   }
                   
                        var allFilesList = _fileDeserializer.DeserializeNecessaryFiles(fromYear, toYear);
                   if (allFilesList.Count != 0)
                    {
                       var listOfChoosedCompanys = allFilesList.Where(a => a.Name == answer).ToList();

                        var commandDispatcher=new CommandDispatcher();
                        var typeOfCalculation = commandDispatcher.OperationList.SingleOrDefault(x => x.Name == calculationName);

                        var companyList=typeOfCalculation.ToSumCompanyRevenue(listOfChoosedCompanys);
                        sumOfRevenue = companyList.First().Value;
                        if (sumOfRevenue != 0)
                        {
                            Console.WriteLine("Firma " + answer + ", przychod  " + sumOfRevenue +
                                              ", okres " +
                                              fromYear + "-" + toYear + "\n");
                        }
                        else
                        {
                            Console.WriteLine("Nie znaleziono danych dla podanej firmy.\n");
                        }
                    }
                    else
                   {
                       Console.WriteLine("Podano bledny zakres lat.\n");
                   }
               }
           }
       }

    
    }
}
