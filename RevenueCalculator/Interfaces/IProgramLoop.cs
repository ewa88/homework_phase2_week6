namespace RevenueCalculator.Interfaces
{
    internal interface IProgramLoop
    {
        void Start();
    }
}