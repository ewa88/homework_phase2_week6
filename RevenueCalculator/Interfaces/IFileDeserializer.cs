using System.Collections.Generic;
using CompanyRevenueDistribution;

namespace RevenueCalculator.Interfaces
{
    internal interface IFileDeserializer
    {
        List<CompanyRevenue> DeserializeJsonFiles();
        List<CompanyRevenue> DeserializeNecessaryFiles(int fromYear, int toYear);
    }
}