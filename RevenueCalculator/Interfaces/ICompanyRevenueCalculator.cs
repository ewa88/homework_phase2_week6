using System.Collections.Generic;

namespace RevenueCalculator.Interfaces
{
    internal interface ICompanyRevenueCalculator
    {
        void CalculateTotalRevenue(string choice);
        void PrintAllCompanysRevenueSync();
        void GetCompanyRevenueData(string calculationName);
    }
}