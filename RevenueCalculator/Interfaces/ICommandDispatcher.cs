using System.Collections.Generic;

namespace RevenueCalculator.Interfaces
{
    internal interface ICommandDispatcher
    {
        IEnumerable<string> GetCalculationNames();
        string GetCommand();
    }
}