﻿using System;
using System.Text.RegularExpressions;

namespace RevenueCalculator
{
   internal class StringFromRegex
    {
        public string GetStringFromRegex()
        {
            string nextIntervalOfYear = "";
            bool regexLoop = true;
            while (regexLoop)
            {              
                    Console.WriteLine(
                        "Wpisz przedział w latach wg wzoru np. 1970-1971 (przychody sa dostepne z lat 1970-2017).");
                    nextIntervalOfYear = Console.ReadLine();

                    Regex nextRegex = new Regex(@"^(\d{4})-(\d{4})*$");
                    if (!nextRegex.IsMatch(nextIntervalOfYear))
                    {
                        Console.WriteLine("Niepoprawnie wpisano przedział w latach. ");
                    }
                    else
                    {
                        regexLoop = false;
                    }
            }
            return nextIntervalOfYear;
        }
    }
}
