﻿using System;
using RevenueCalculator.Interfaces;

namespace RevenueCalculator
{
    internal class ProgramLoop : IProgramLoop
    {
        private ICompanyRevenueCalculator _companyRevenueCalculator;
        private ICommandDispatcher _commandDispatcher;

        public ProgramLoop(ICompanyRevenueCalculator companyRevenueCalculator, ICommandDispatcher commandDispatcher)
        {
            _companyRevenueCalculator = companyRevenueCalculator;
            _commandDispatcher = commandDispatcher;
        }
        public void Start()
        {
            bool loop = true;
            while (loop)
            {
                Console.WriteLine(
                    "Wpisz komende: \n1 - suma przychodów wszystkich firm,\n" +
                    "2 - suma przychodów wybranej firmy w danym okresie,\nwpisz exit jesli chcesz zakonczyc.");
                string answer = Console.ReadLine();
                switch (answer)
                {
                    case "1":
                        GetRevenueAllCompanys();
                        break;

                    case "2":
                        GetRevenueSelectedCompany();
                        break;

                    case "exit":
                        loop = false;
                        break;

                    case "Exit":
                        loop = false;
                        break;

                    default:
                        Console.WriteLine("Podana komenda nie istnieje");
                        break;
                }
            }
            
        }

        private void GetRevenueSelectedCompany()
        {
            string choice = _commandDispatcher.GetCommand();
             _companyRevenueCalculator.GetCompanyRevenueData(choice);
        }

        public void GetRevenueAllCompanys()
        {
            string choice = _commandDispatcher.GetCommand();
            _companyRevenueCalculator.CalculateTotalRevenue(choice);
        }

     
    }
}
