﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using RevenueCalculator.Interfaces;
using RevenueCalculator.NinjectModule;

namespace RevenueCalculator
{
    internal class Program
    {     
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new ProgramModule());
            kernel.Get<IProgramLoop>().Start();
        }
    }
}
