﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompanyRevenueDistribution;

namespace RevenueCalculator
{
   internal class RevenueAddition : IRevenueAddition
   {
       public string Name => "ObliczPrzychodCalkowity";
        public List<CompanyRevenue> ToSumCompanyRevenue(List<CompanyRevenue> listOfChoosedCompanys)
        {
            List<CompanyRevenue> companyList = new List<CompanyRevenue>();
            foreach (var data in listOfChoosedCompanys)
            {
                var foundValue = companyList.SingleOrDefault(d => d.Name == data.Name);
                if (foundValue != null)
                {
                    foundValue.Value += data.Value;
                }
                else
                {
                    companyList.Add(data);
                }
            }
            return companyList;
        }
    }
}
