﻿using CompanyRevenueDistribution;
using RevenueCalculator.Interfaces;

namespace RevenueCalculator.NinjectModule
{
    internal class ProgramModule : Ninject.Modules.NinjectModule
{
    public override void Load()
    {
        Bind<IProgramLoop>().To<ProgramLoop>();
        Bind<IFileDeserializer>().To<FileDeserializer.FileDeserializer>();
        Bind<ICompanyRevenueCalculator>().To<CompanyRevenueCalculator>();
        Bind<IRevenueAddition>().To<RevenueAddition>();
        Bind<ICommandDispatcher>().To<CommandDispatcher>();
        }
}
}
