using System.Collections.Generic;

namespace CompanyRevenueDistribution
{
    public interface IRevenueAddition
    {
        string Name { get; }
        List<CompanyRevenue> ToSumCompanyRevenue(List<CompanyRevenue> listOfChoosedCompanys);
    }
}