﻿using System;

namespace CompanyRevenueDistribution
{
    public class CompanyRevenue
    {
        public string Name;
        public DateTime Date;
        public long Value;
    }
}
