﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompanyRevenueDistribution;

namespace RevenueAveragePlugin
{
    public class RevenueAverage:IRevenueAddition
    {
        public string Name => "ObliczSredniPrzychod";
        public List<CompanyRevenue> ToSumCompanyRevenue(List<CompanyRevenue> listOfChoosedCompanys)
        {          
            Dictionary<string, int> valueRegisterCount = new Dictionary<string, int>(); 
            List<CompanyRevenue> companyList = new List<CompanyRevenue>();
            foreach (var data in listOfChoosedCompanys)
            {
                var foundValue = companyList.SingleOrDefault(d => d.Name == data.Name);
                if (foundValue != null)
                {
                    foundValue.Value += data.Value;
                    valueRegisterCount[data.Name] = valueRegisterCount[data.Name] + 1;
                }
                else
                {
                    companyList.Add(data);
                    valueRegisterCount.Add(data.Name, 1);
                }
            }
            foreach (var company in companyList)
            {
                if (valueRegisterCount.ContainsKey(company.Name))
                    company.Value = company.Value / valueRegisterCount[company.Name];
            }
            return companyList;
        }

    }
}
